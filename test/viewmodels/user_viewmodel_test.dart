import 'package:flutter_test/flutter_test.dart';
import 'package:sample_api_app/app/app.locator.dart';

import '../helpers/test_helpers.dart';

void main() {
  group('UserViewModel Tests -', () {
    setUp(() => registerServices());
    tearDown(() => locator.reset());
  });
}
