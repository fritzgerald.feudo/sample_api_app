import 'package:flutter/material.dart';

class HomeEntryTile extends StatelessWidget {
  final String title;
  final void Function()? onTap;

  const HomeEntryTile({this.title = '', this.onTap, super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          title,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
