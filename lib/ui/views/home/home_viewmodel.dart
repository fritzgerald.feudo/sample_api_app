import 'package:sample_api_app/app/app.locator.dart';
import 'package:sample_api_app/app/app.router.dart';
import 'package:sample_api_app/models/user.dart';
import 'package:sample_api_app/services/api_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends FutureViewModel<List<User>> {
  final _apiService = locator<ApiService>();
  final _navigationService = locator<NavigationService>();

  @override
  Future<List<User>> futureToRun() => _apiService.getUsers();

  Future viewUserDetails({required User user}) =>
      _navigationService.replaceWithUserView(userId: user.id);


}
