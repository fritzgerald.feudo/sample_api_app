import 'package:flutter/material.dart';
import 'package:sample_api_app/ui/views/home/widgets/home_entry_tile.dart';
import 'package:stacked/stacked.dart';

import 'home_viewmodel.dart';

class HomeView extends StackedView<HomeViewModel> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    HomeViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Home',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 25.0),
        child: viewModel.dataReady && (viewModel.data?.isNotEmpty ?? false)
            ? ListView.builder(
                itemCount: viewModel.data!.length,
                itemBuilder: (context, index) {
                  final user = viewModel.data![index];

                  return HomeEntryTile(
                    title: user.name,
                    onTap: () async =>
                        await viewModel.viewUserDetails(user: user),
                  );
                })
            : const Center(child: CircularProgressIndicator()),
      ),
    );
  }

  @override
  HomeViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      HomeViewModel();
}
