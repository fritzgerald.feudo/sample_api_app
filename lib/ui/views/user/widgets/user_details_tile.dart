import 'package:flutter/material.dart';

class UserDetailsTile extends StatelessWidget {
  final String name;
  final String username;
  final String email;

  const UserDetailsTile(
      {this.name = '', this.username = '', this.email = '', super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          _EntryTile('Name', name),
          _EntryTile('Username', username),
          _EntryTile('Email', email),
        ],
      ),
    );
  }
}

class _EntryTile extends StatelessWidget {
  final String label;
  final String value;

  const _EntryTile(
    this.label,
    this.value,
  );

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Text(
        label,
        style: const TextStyle(
          fontSize: 20,
        ),
      ),
      title: Text(
        value,
        style: const TextStyle(
          fontSize: 20,
        ),
      ),
    );
  }
}
