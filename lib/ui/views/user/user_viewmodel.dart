import 'package:sample_api_app/app/app.bottomsheets.dart';
import 'package:sample_api_app/app/app.locator.dart';
import 'package:sample_api_app/app/app.router.dart';
import 'package:sample_api_app/models/user.dart';
import 'package:sample_api_app/services/api_service.dart';
import 'package:sample_api_app/ui/bottom_sheets/option/option_sheet_model.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class UserViewModel extends FutureViewModel<User> {
  final _navigationService = locator<NavigationService>();
  final _apiService = locator<ApiService>();
  final _bottomSheetService = locator<BottomSheetService>();

  final int id;

  UserViewModel({required this.id});

  @override
  Future<User> futureToRun() async {
    final user = await _apiService.getUserById(id: id);

    if (user == null) {
      await _navigationService.replaceWithHomeView();
    }

    return user!;
  }

  Future goBack() async => _navigationService.replaceWithHomeView();

  Future<void> showBottomSheet() async {
    final option = await _bottomSheetService.showCustomSheet(
      variant: BottomSheetType.option,
      title: 'View',
      description: 'No options available',
      data: OptionSheetModel(options: [
        const OptionSheetOption(label: 'Todos', value: 1),
      ]),
    );



    switch(option?.data) {
      case 1:
        await _navigationService.replaceWithTodoView(userId: id, name: data?.name);
      default:
        break;
    }
  }
}
