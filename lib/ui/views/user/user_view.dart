import 'package:flutter/material.dart';
import 'package:sample_api_app/ui/common/widgets/loading_indicator.dart';
import 'package:sample_api_app/ui/views/user/widgets/user_details_tile.dart';
import 'package:stacked/stacked.dart';

import 'user_viewmodel.dart';

class UserView extends StackedView<UserViewModel> {
  final int userId;

  const UserView({required this.userId, Key? key}) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    UserViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () async {
            await viewModel.goBack();
          },
        ),
      ),
      body: viewModel.dataReady
          ? ListView(
              children: [
                UserDetailsTile(
                  name: viewModel.data?.name ?? '',
                  username: viewModel.data?.username ?? '',
                  email: viewModel.data?.email ?? '',
                )
              ],
            )
          : const LoadingIndicator(),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.view_agenda),
        onPressed: () {
          viewModel.showBottomSheet();
        },
      ),
    );
  }

  @override
  UserViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      UserViewModel(id: userId);
}
