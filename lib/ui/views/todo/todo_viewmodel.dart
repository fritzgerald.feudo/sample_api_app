import 'package:sample_api_app/app/app.locator.dart';
import 'package:sample_api_app/app/app.router.dart';
import 'package:sample_api_app/models/todo.dart';
import 'package:sample_api_app/services/api_service.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class TodoViewModel extends FutureViewModel<List<Todo>> {
  final _apiService = locator<ApiService>();
  final _navigationService = locator<NavigationService>();

  final int? userId;
  final String? name;

  TodoViewModel({this.userId, this.name});

  @override
  Future<List<Todo>> futureToRun() async {
    var todos = <Todo>[];
    if (userId == null) {
      todos = await _apiService.getTodos();
    } else {
      todos = await _apiService.getTodosByUserId(userId: userId!);
    }
    return todos;
  }

  Future goBack() async {
    if (userId != null) {
      return _navigationService.replaceWithUserView(userId: userId!);
    }

    return _navigationService.replaceWithHomeView();
  }
}
