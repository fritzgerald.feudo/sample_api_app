import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'todo_viewmodel.dart';

class TodoView extends StackedView<TodoViewModel> {
  final int? userId;
  final String? name;

  const TodoView({
    this.userId,
    this.name,
    Key? key
  }) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    TodoViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () async {
            await viewModel.goBack();
          },
        ),
        title: ListTile(
          title: const Text(
            "Todos",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            name ?? '',
            style: const TextStyle(
              fontSize: 18
            ),
          ),
        ),
      ),
      body: viewModel.dataReady && (viewModel.data?.isNotEmpty ?? false)
          ? ListView.builder(
              itemCount: viewModel.data!.length,
              itemBuilder: (context, index) {
                final data = viewModel.data![index];

                return Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: ListTile(
                    title: Text(
                      data.title,
                      style: const TextStyle(
                      ),
                    ),
                    trailing: Icon(
                      data.completed
                          ? Icons.check_box
                          : Icons.check_box_outline_blank,
                    ),
                  ),
                );
              },
            )
          : const Center(
              child: CircularProgressIndicator(),
            ),
    );
  }

  @override
  TodoViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      TodoViewModel(userId: userId, name: name);
}
