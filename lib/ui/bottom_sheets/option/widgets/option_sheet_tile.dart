import 'package:flutter/material.dart';

class OptionSheetTile extends StatelessWidget {
  final String label;
  final Function()? onTap;
  final Color? color;

  const OptionSheetTile({
    this.label = '',
    this.onTap,
    this.color,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: color,
      child: ListTile(
        title: Center(
          child: Text(
            label,
            style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
