import 'package:flutter/material.dart';
import 'package:sample_api_app/ui/bottom_sheets/option/widgets/option_sheet_tile.dart';
import 'package:sample_api_app/ui/common/app_colors.dart';
import 'package:sample_api_app/ui/common/ui_helpers.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import 'option_sheet_model.dart';

class OptionSheet extends StackedView<OptionSheetModel> {
  final Function(SheetResponse response)? completer;
  final SheetRequest request;
  const OptionSheet({
    Key? key,
    required this.completer,
    required this.request,
  }) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    OptionSheetModel viewModel,
    Widget? child,
  ) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(10),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            request.title ?? '',
            style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w900,
            ),
            textAlign: TextAlign.center,
          ),
          if (viewModel.options.isNotEmpty)
            ...viewModel.options.map((entry) => OptionSheetTile(
              label: entry.label,
              onTap: () {
                completer!(SheetResponse(data: entry.value));
              },
            )).toList(),
          if (viewModel.options.isEmpty && request.description != null)
            ...[
              verticalSpaceTiny,
              Text(
                request.description!,
                style: const TextStyle(fontSize: 14, color: kcMediumGrey),
                maxLines: 3,
                softWrap: true,
              ),
            ],
          OptionSheetTile(
            color: kcMediumGrey,
            label: 'Hide',
            onTap: () {
              completer!(SheetResponse(data: viewModel.cancelValue));
            },
          )
        ],
      ),
    );
  }

  @override
  OptionSheetModel viewModelBuilder(BuildContext context) {
    try {
      return request.data as OptionSheetModel;
    } catch (e) {
      debugPrint('Option sheet: No options available');
    }

    return OptionSheetModel();
  }
}
