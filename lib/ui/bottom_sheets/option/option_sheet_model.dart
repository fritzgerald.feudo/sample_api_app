import 'package:stacked/stacked.dart';

class OptionSheetModel extends BaseViewModel {
  final List<OptionSheetOption> options;
  final dynamic cancelValue;

  OptionSheetModel({this.options = const [], this.cancelValue = 0});
}

class OptionSheetOption {
  final String label;
  final dynamic value;

  const OptionSheetOption({
    required this.label,
    required this.value,
  });
}