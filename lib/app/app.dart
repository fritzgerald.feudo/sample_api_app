import 'package:sample_api_app/ui/views/home/home_view.dart';
import 'package:sample_api_app/ui/views/startup/startup_view.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:sample_api_app/ui/views/todo/todo_view.dart';
import 'package:sample_api_app/services/api_service.dart';
import 'package:sample_api_app/services/mock_data_service.dart';
import 'package:sample_api_app/ui/views/user/user_view.dart';
import 'package:sample_api_app/ui/bottom_sheets/option/option_sheet.dart';
// @stacked-import

@StackedApp(
  routes: [
    MaterialRoute(page: HomeView),
    MaterialRoute(page: StartupView),
    MaterialRoute(page: TodoView),
    MaterialRoute(page: UserView),
// @stacked-route
  ],
  dependencies: [
    LazySingleton(classType: BottomSheetService),
    LazySingleton(classType: DialogService),
    LazySingleton(classType: NavigationService),
    LazySingleton(classType: ApiService),
    LazySingleton(classType: MockDataService),
// @stacked-service
  ],
  bottomsheets: [
    StackedBottomsheet(classType: OptionSheet),
    // @stacked-bottom-sheet
  ],
)
class App {}
