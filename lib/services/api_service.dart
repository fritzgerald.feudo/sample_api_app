import 'dart:convert';
import 'package:flutter/foundation.dart';
// import 'package:sample_api_app/app/app.locator.dart';
import 'package:sample_api_app/models/todo.dart';
import 'package:http/http.dart' as http;
import 'package:sample_api_app/models/user.dart';
// import 'package:sample_api_app/services/mock_data_service.dart';

class ApiService {
  static const _domain = 'jsonplaceholder.typicode.com';
  // final _mockDataService = locator<MockDataService>();

  Future<List<Todo>> getTodos() async {
    try {
      var url = Uri.https(_domain, 'todos');
      final response = await http.get(url);

      if (response.statusCode == 200) {
        final decoded = jsonDecode(response.body);
        final todos = <Todo>[];
        for (final json in decoded) {
          todos.add(Todo.fromJson(json));
        }
        return todos;
      } else {
        throw Exception('Returned ${response.statusCode}.');
      }
    } catch (e) {
      debugPrint("Failed to get todos through API call: $e");
      // debugPrint("Returning Mock data...");

      /// Uncomment this if you want to use mock data on error or empty list
      // final decoded = jsonDecode(_mockDataService.todos);
      //
      // final todos = <Todo>[];
      // for (final i in decoded) {
      //   todos.add(Todo.fromJson(i));
      // }
      // return todos;

      return [];
    }
  }

  Future<List<Todo>> getTodosByUserId({required int userId}) async {
    try {
      var url = Uri.https(_domain, 'todos', {'userId' : userId.toString()});
      final response = await http.get(url);


      if (response.statusCode == 200) {
        final decoded = jsonDecode(response.body);
        final todos = <Todo>[];
        for (final json in decoded) {
          todos.add(Todo.fromJson(json));
        }
        return todos;
      } else {
        throw Exception('Returned ${response.statusCode}.');
      }
    } catch (e) {
      debugPrint("Failed to get todos through API call: $e");

      return [];
    }
  }

  Future<List<User>> getUsers() async {
    try {
      var url = Uri.https(_domain, 'users');
      final response = await http.get(url);

      if (response.statusCode == 200) {
        final decoded = jsonDecode(response.body);
        final users = <User>[];
        for (final json in decoded) {
          users.add(User.fromJson(json));
        }
        return users;
      } else {
        throw Exception('Returned ${response.statusCode}.');
      }
    } catch (e) {
      debugPrint("Failed to get todos through API call: $e");
      return [];
    }
  }

  Future<User?> getUserById({required id}) async {
    try {
      var url = Uri.https(_domain, 'users/$id');
      final response = await http.get(url);

      if (response.statusCode == 200) {
        final decoded = jsonDecode(response.body);
        return User.fromJson(decoded);
      } else {
        throw Exception('Returned ${response.statusCode}.');
      }
    } catch (e) {
      debugPrint("Failed to get todos through API call: $e");
      return null;
    }
  }
}
